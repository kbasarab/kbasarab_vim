# If you come from bash you might have to change your $PATH.
export PATH=$HOME/bin:$HOME/.composer/vendor/bin:/usr/local/sbin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH=/Users/kevinbasarab/.oh-my-zsh

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
#ZSH_THEME="robbyrussell"
ZSH_THEME="agnoster_custom"
# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
ZSH_CUSTOM=~/resources/zsh_custom

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

alias docker-stop='docker stop $(docker ps -aq)'
alias docker-clean='docker image prune -a'
alias ddev-rm-f='ddev rm --remove-data --omit-snapshot'
alias gitinfo='~/resources/bash/git-info.sh'
alias avd='/Users/kevinbasarab/www/android/./tools/android avd'
alias ls='ls -G'
alias combine_pdf='ls *.pdf | sort | xargs -J % pdftk % cat output all.pdf';
alias drupalcs="phpcs --standard=Drupal,DrupalPractice --extensions='php,module,inc,install,test,profile,theme,js,css,info,txt'";
alias drupalcbf="phpcbf --standard=Drupal --extensions='php,module,inc,install,test,profile,theme,js,css,info,txt'";
alias git-prune='git branch --merged | egrep -v "(^\*|master|dev|main|develop)" | xargs git branch -d'

alias docker-bb-mysql="docker run --network=host --name bb_mysql  -e MYSQL_DATABASE='db' -e MYSQL_RANDOM_ROOT_PASSWORD='db' -e MYSQL_USER='db' -e MYSQL_PASSWORD='db' -d mysql:8.0"
alias docker-bb="docker run --network=host -v `pwd`:/www -it --memory=4g --memory-swap=4g --name bb_pipelines --memory-swappiness=0 --cpus=1 --entrypoint=/bin/bash mediacurrent/mc-bb-pipelines:PHP8.0"


export SVN_EDITOR=vim
export EDITOR=vim

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

alias resetaudio="ps aux | grep 'coreaudio[d]' | awk '{print $2}' | xargs sudo kill"

# PHP Error check
phpco() { docker run --init -v $PWD:/mnt/src:cached --rm -u "$(id -u):$(id -g)" frbit/phpco:latest $@; return $?; }


# Commands to run when agent forwarding isn't working in this order.
eval `ssh-agent`

ssh-add &>/dev/null || eval `ssh-agent` &>/dev/null  # start ssh-agent if not present
[ $? -eq 0 ] && {
  ssh-add ~/.ssh/id_dsa
  ssh-add ~/.ssh/id_rsa_4096
  ssh-add ~/resources/aws/kbasarab.pem
#  ssh-add ~/resources/aws/wwedev.pem
}

# Drush autocompletion.
autoload bashcompinit
bashcompinit
source ~/resources/drush_complete.sh

export NVM_DIR=~/.nvm
source $(brew --prefix nvm)/nvm.sh

#PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# Adds ~/resources/bin to $PATH
PATH=$PATH:$HOME/resources/bin

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/kevinbasarab/google-cloud-sdk/path.zsh.inc' ]; then . '/Users/kevinbasarab/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/Users/kevinbasarab/google-cloud-sdk/completion.zsh.inc' ]; then . '/Users/kevinbasarab/google-cloud-sdk/completion.zsh.inc'; fi
export PATH="/usr/local/opt/ruby/bin:/usr/local/lib/ruby/gems/2.7.0:$PATH"

# added by Snowflake SnowSQL installer v1.2
export PATH=/Applications/SnowSQL.app/Contents/MacOS:$PATH

# Flutter SDK
export PATH="$PATH:/Users/kevinbasarab/www/flutter_sdk/bin"
function blt() {
  if [[ ! -z ${AH_SITE_ENVIRONMENT} ]]; then
    PROJECT_ROOT="/var/www/html/${AH_SITE_GROUP}.${AH_SITE_ENVIRONMENT}"
  elif [ "`git rev-parse --show-cdup 2> /dev/null`" != "" ]; then
    PROJECT_ROOT=$(git rev-parse --show-cdup)
  else
    PROJECT_ROOT="."
  fi

  if [ -f "$PROJECT_ROOT/vendor/bin/blt" ]; then
    $PROJECT_ROOT/vendor/bin/blt "$@"

  # Check for local BLT.
  elif [ -f "./vendor/bin/blt" ]; then
    ./vendor/bin/blt "$@"

  else
    echo "You must run this command from within a BLT-generated project."
    return 1
  fi
}
