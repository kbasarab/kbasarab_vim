<?php
/**
 * @file
 * Aliases downloaded from Acquia UI.
 */
if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site fhcookbookmc, environment dev
$aliases['dev'] = array(
  'root' => '/var/www/html/fhcookbookmc.dev/docroot',
  'ac-site' => 'fhcookbookmc',
  'ac-env' => 'dev',
  'ac-realm' => 'devcloud',
  'uri' => 'fhcookbookmcppak6javtw.devcloud.acquia-sites.com',
  'remote-host' => 'free-3891.devcloud.hosting.acquia.com',
  'remote-user' => 'fhcookbookmc.dev',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['dev.livedev'] = array(
  'parent' => '@fhcookbookmc.dev',
  'root' => '/mnt/gfs/fhcookbookmc.dev/livedev/docroot',
);

// Site fhcookbookmc, environment test
$aliases['test'] = array(
  'root' => '/var/www/html/fhcookbookmc.test/docroot',
  'ac-site' => 'fhcookbookmc',
  'ac-env' => 'test',
  'ac-realm' => 'devcloud',
  'uri' => 'fhcookbookmcuhryszbg7e.devcloud.acquia-sites.com',
  'remote-host' => 'free-3891.devcloud.hosting.acquia.com',
  'remote-user' => 'fhcookbookmc.test',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['test.livedev'] = array(
  'parent' => '@fhcookbookmc.test',
  'root' => '/mnt/gfs/fhcookbookmc.test/livedev/docroot',
);
