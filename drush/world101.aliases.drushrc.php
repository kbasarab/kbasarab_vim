<?php

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}

#include trailing slash for sites directory
$vag_sites = '/var/www/world101/';

$aliases['w101'] = array(
  'env' => 'w101',
  'root' => $vag_sites . 'src/web',
  'remote-host' => 'world101.dev',
  'remote-user' => 'vagrant',
  'uri' => 'world101.dev',
  'path-aliases' => array(
    '%dump-dir' => $vag_sites . 'src/backups',
    '%files' => $vag_sites . 'src/sites/default/files'
  )
);
