<?php

$aliases['d7.local'] = array(
   'uri' => 'd7.local',
   'remote-host' => 'd7.local',
   'remote-user' => 'vagrant',
   'ssh-options' => '-i /Users/kbasarab/.vagrant.d/insecure_private_key -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no',
   'root' => '/var/beetbox/docroot',
);
