<?php
/**
 * Place this file in your ~./drush directory. Create it
 * if you don't have one yet.
 * $cd ~; mkdir .drush
 */

/**
 * Drush commands will be in the form of from-environment -> to-environment
 *
 * Example usage to sync your local copy.
 * Use rsync to sync files from dev to local.
 * $ drush rsync @alaskad7.dev:%files @alaskad7.local:%files
 *
 * Want to copy DB from dev to local?
 * $ drush sql-sync --no-cache @alaskad7.dev @alaskad7.local
 * $ drush sql-sync --no-cache @alaskad7.dev @alaskad7.stage
 *
 * Because %dump-dir is defined for local a copy will be left there.
 * You need "--no-cache" unless you have command-specific sql-synch no-cache
 * defined because otherwise it will pull the latest dump made in the last
 * 24 hours which obviously may be out of date.
 *
 * After adding the command-specific no-cache you can just use:
 * $ drush sql-sync @alaskad7.dev @alaskad7.local
 */

// Site lifeally2012, environment prod
/*$aliases['alaska.prod'] = array(
  'parent' => '@alaska.staging',
  'root' => '/var/www/html/alaskadispatch.prod/docroot',
  'remote-host' => 'ded-446.prod.hosting.acquia.com',
  'remote-user' => 'alaskadispatch',
  'target-command-specific' => array (
    'sql-sync' => array (
      'simulate' => '1',
    ),
    'rsync' => array (
      'simulate' => '1',
    ),
  ),
);
*/
// Site alaskad7, environment local
$aliases['alaskad7.local'] = array(
  'root' => '/Users/kbasarab/www/alaska.d7/docroot/',
  'uri' => 'alaska.d7',
  'path-aliases' => array(
    '%files' => 'sites/default/files',
     // Path for sql-sync dumps.
  ),
  'skip-tables' => array('users', 'users_roles', 'roles', 'permission'),
);

$aliases['alaskad7.aws'] = array(
  'root' => '/var/www/alaska.aws/docroot/',
  'uri' => 'alaska.aws',
  'remote-host' => 'kb.aws',
  'path-aliases' => array(
    '%files' => 'sites/default/files',
     // Path for sql-sync dumps.
  ),
  'skip-tables' => array('users', 'users_roles', 'roles', 'permission'),
);

