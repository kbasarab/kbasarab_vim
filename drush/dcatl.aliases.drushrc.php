<?php

$aliases['dcatl.local'] = array(
  'root' => '/Users/kbasarab/www/sites/dcatl.2013',
  'uri' => 'dcatl.2013',
  'path-aliases' => array(
    '%files' => 'sites/default/files',
    // Path for sql-sync dumps.
    '%dump' => '/Users/kbasarab/www/drush-dumps/test_dump-' . date('Ymd-His') . '.sql', // create dated DB dumps.
  ),
);;
