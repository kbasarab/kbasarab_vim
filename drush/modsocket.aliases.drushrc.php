<?php
/**
 * Place this file in your ~./drush directory. Create it
 * if you don't have one yet.
 * $cd ~; mkdir .drush
 */

/**
 * Drush commands will be in the form of from-environment -> to-environment
 *
 * Example usage to sync your local copy.
 * Use rsync to sync files from prod to local, or dev to local
 * $ drush rsync @modsocket.prod:%files @modsocket.local:%files
 * $ drush rsync @modsocket.dev:%files @modsocket.local:%files
 *
 * Want to copy DB from dev to local?
 * $ drush sql-sync --no-cache @modsocket.dev @modsocket.local
 *
 * Because %dump-dir is defined for local a copy will be left there.
 * You need "--no-cache" unless you have command-specific sql-synch no-cache
 * defined because otherwise it will pull the latest dump made in the last
 * 24 hours which obviously may be out of date.
 *
 * After adding the command-specific no-cache you can just use:
 * $ drush sql-sync @modsocket.dev @modsocket.local
 * $ drush sql-sync @modsocket.prod @modsocket.local
 *
 */

// Site dfarmer, environment development
$aliases['modsocket.dev'] = array(
  'parent' => '@modsocket.dev',
  'root' => '/var/www/html/dfarmerdev/docroot',
  'remote-host' => 'srv-1060.devcloud.hosting.acquia.com',
  'remote-user' => 'dfarmer',
  'uri' => 'dfarmerdev.devcloud.acquia-sites.com',
  'path-aliases' => array(
    '%files' => 'files',
    '%dump-dir' => '/tmp',
  ),
  // Add no-cache option to make sure you are getting a new DB snapshot
  'command-specific' => array (
    'sql-sync' => array (
      'no-cache' => TRUE,
    ),
  ),
);

// Site dfarmer, environment production
$aliases['modsocket.prod'] = array(
  'parent' => '@modsocket.dev',
  'root' => '/var/www/html/dfarmer/docroot',
  'uri' => 'www.modsocket.com',
  // We want to prevent accidental overwrites to Production so we enforce
  // 'simulate' for sql-sync and rsync if the target environment is production.
  'target-command-specific' => array (
    'sql-sync' => array (
      'simulate' => '1',
    ),
    'rsync' => array (
      'simulate' => '1',
    ),
  ),
);

// Site dfarmer, environment test/staging
$aliases['modsocket.stage'] = array(
  'parent' => '@modsocket.dev',
  'root' => '/var/www/html/dfarmertest/docroot',
  'uri' => 'dfarmertest.devcloud.acquia-sites.com',
);

// Site dfarmer, environment local
$aliases['modsocket.local'] = array(
  'root' => '/Users/kbasarab/www/sites/modsocket.kb', // local path to docroot
  'uri' => 'modsocket.kb',
  'path-aliases' => array(
    '%files' => 'files',
     // Optional: path for sql-sync dumps if you want to keep them.
    '%dump' => '/Users/kbasarab/www/drush-dumps/modsocket_dev-' . date('Ymd-His') . '.sql',
  ),
  // Optional: sanitize the DB dump.
  /*
  'target-command-specific' => array (
    'sql-sync' => array (
      'sanitize' => '1',
    ),
 ),
  */
);
