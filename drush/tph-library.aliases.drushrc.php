<?php

/**
 * Replacement strings:
 * Look for strings wrapped in { } and update them to match your environments.
 * e.g. tph-lib is the abreviation name to replace throughout.
 */

/**
 * Site aliases for tph-lib
 * Place this file at ~/.drush/  (~/ means your home path)
 *
 * Some protection has been added to prevent accidental harm to production
 * environments but please be careful!
 *
 * Drush commands are in the form of source-environment -> target-environment
 */

/**
 * Rsync Examples
 *
 * rsync Usage Examples:
 * To sync files from the production site to your local site:
 * $ drush rsync @tph-lib.prod:%files @tph-lib.local:%files
 *
 * To sync files from the dev site to your local site:
 * $ drush rsync @tph-lib.dev:%files @tph-lib.local:%files
 */

 /**
 * DB Sync Examples
 *
 * Cache Data:
 * Excluding cache table data is set as a default but the table list should be
 * updated periodically for projects under development.
 *
 * Sanitization:
 * You can sanitize (scramble) user email and passwords for security, to prevent
 * spam from test updates, etc. This is done after the db has been downloaded if
 * the target is remote (normally).
 * See note in the local alias below to enable sanitization. Once enabled you
 * you will be asked if you wish to sanitize the users after a local target sync.
 *
 * DB Dumps:
 * sql-sync db dumps are normally cached for 24hrs and this has been overridden
 * in this file so only fresh data is used.
 * Dumps are sent to /tmp but can be changed in the below config.
 * If you wish to retain dated sql dump files for backups see the note in the
 * local site alias below.
 *
 * sql-sync Usage Examples:
 * To copy the DB from the dev site to your local build:
 * $ drush sql-sync @tph-lib.dev @tph-lib.local
 *
 * From production to your local build:
 * $ drush sql-sync @tph-lib.prod @tph-lib.local
 * $ drush sql-sync @tph-lib.dev @tph-lib.local
 */

/**
 * Local alias
 * Set the root, uri and path values to point to your local install.
 * Enable optional settings if desired.
 */
$aliases['tph-lib.local'] = array(
  // local path to docroot
  'root' => '/Users/kbasarab/www/sites/tephinet.lib',
  'uri' => 'tephinet.lib',
  'path-aliases' => array(
    '%dump-dir' => '/tmp',
    '%files' => 'sites/default/files',
    // Optional: Uncomment and specify a local path for retained db dumps.
    #'%dump' => '~/Sites/drush-dumps/tph-lib-'. date('Ymd-His') .'.sql',
  ),
  // Optional: Uncomment the sanitze option to sanitize the DB dump post sync.
  // You will be given a y/n option for this.
  'target-command-specific' => array (
    'sql-sync' => array (
      #'sanitize' => '1',
      'confirm-sanitizations' => '1',
    ),
  ),
);

// Development
$aliases['tph-lib.stage'] = array(
  'remote-host' => 'mcstaging',
  #'remote-user' => '{example-user}',
  'site' => 'tephinet-lib',
  'root' => '/opt/lampp/htdocs/staging/tephinet_lib',
  'uri' => 'tephinet-lib.mediacurrentstaging.info',
  'path-aliases' => array(
    '%dump-dir' => '/tmp',
    '%files' => 'sites/default/files',
  ),
  'command-specific' => array (
    'sql-sync' => array (
      // Add no-cache option to make sure you are getting a new DB snapshot
     'no-cache' => TRUE,
      // The --no-ordered-dump option can help reduce the dump size.
      '--no-ordered-dump' => TRUE,
      // Exclude cache and other trashable table data to reduce overhead.
      'structure-tables' => array(
        'common' => array(
          // Skip tables
          'cache_apachesolr',
          'cache_block',
          'cache_content',
          'cache_filter',
          'cache_form',
          'cache_location',
          'cache_menu',
          'cache_page',
          'cache_rules',
          'cache_update',
          'cache_views_data',
          'cache_views',
          'cache',
          'history',
          'search_dataset',
          'search_index',
          'search_node_links',
          'search_total',
          'sessions',
          'views_object_cache',
          'votingapi_cache',
          'watchdog',
        ),
      ),
      // Uncomment to make this a default option.
      'structure-tables-key' => 'common',
    ),
  ),
);

// Production
$aliases['tph-lib.prod'] = array(
  'parent' => '@tph-lib.stage',
  'remote-host' => '174.143.173.63',
  'remote-user' => 'root',
  // This ssh option allows you to proxy through an intermediary server to the
  // destination host. Useful if the destination server has restricted access.
  // netcat is needed on the intermediary and you will need ssh agent forwarding.
  'ssh-options' => '-o "ProxyCommand ssh mcstaging nc %h %p 2> /dev/null"',
  'root' => '/opt/lampp/htdocs/tephinet_lib',
  'uri' => 'library.tephinet.org',
  'path-aliases' => array(
    '%dump-dir' => '/tmp',
    '%files' => 'sites/default/files',
    '%drush' => '/usr/local/drush',
    '%site' => 'sites/default/',
  ),
  // We want to prevent accidental overwrites to Production so we enforce
  // 'simulate' for sql-sync and rsync if the target environment is production.
  'target-command-specific' => array (
    'sql-sync' => array (
      'simulate' => '1',
    ),
    'rsync' => array (
      'simulate' => '1',
    ),
  ),
);
