<?php
/**
 * Place this file in your ~./drush directory. Create it
 * if you don't have one yet.
 * $cd ~; mkdir .drush
 */

/**
 * Drush commands will be in the form of from-environment -> to-environment
 *
 * Example usage to sync your local copy.
 * Use rsync to sync files from dev to local.
 * $ drush rsync @alaskad7.dev:%files @alaskad7.local:%files
 *
 * Want to copy DB from dev to local?
 * $ drush sql-sync --no-cache @alaskad7.dev @alaskad7.local
 * $ drush sql-sync --no-cache @alaskad7.dev @alaskad7.stage
 *
 * Because %dump-dir is defined for local a copy will be left there.
 * You need "--no-cache" unless you have command-specific sql-synch no-cache
 * defined because otherwise it will pull the latest dump made in the last
 * 24 hours which obviously may be out of date.
 *
 * After adding the command-specific no-cache you can just use:
 * $ drush sql-sync @alaskad7.dev @alaskad7.local
 */

// Site glisi, environment dev
$aliases['glisi.dev'] = array(
  'root' => '/home/glisi/dev.glisi.org',
  'remote-host' => 'dev.glisi.org',
  'remote-user' => 'glisi',
  'ssh-options' => ' -p 2222',
  'uri' => 'dev.glisi.org',
  'path-aliases' => array(
    '%drush' => '/home/glisi/drush/drush',
    '%files' => 'sites/default/files',
    '%dump-dir' => '/tmp',
  ),
  'command-specific' => array(
    'sql-sync' => array(
      'no-cache' => TRUE,
//      'sanitize' => TRUE,
      'no-ordered-dump' => TRUE,
      'structure-tables' => array( // Add tables that should have data skipped during sql-sync
        'common' => array('cache', 'cache_block', 'cache_content', 'cache_filter', 'cache_menu', 'cache_page', 'sessions', 'watchdog'),
      ),
    )
  ),
);


// Site glisi environment local
$aliases['glisi.local'] = array(
  'root' => '/Users/kbasarab/www/sites/glisi.d7/',
  'uri' => 'glisi.d7',
  'path-aliases' => array(
    '%files' => 'sites/default/files',
     // Path for sql-sync dumps.
    '%dump' => '/Users/kbasarab/www/drush-dumps/glisi-' . date('Ymd-His') . '.sql',
  ),
);

