<?php

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site alaskadispatch, environment dev
$aliases['dev'] = array(
  'root' => '/var/www/html/alaskadispatch.dev/docroot',
  'ac-site' => 'alaskadispatch',
  'ac-env' => 'dev',
  'ac-realm' => 'prod',
  'uri' => 'alaskadisdev.prod.acquia-sites.com',
  'remote-host' => 'staging-4594.prod.hosting.acquia.com',
  'remote-user' => 'alaskadispatch.dev',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['dev.livedev'] = array(
  'parent' => '@alaskadispatch.dev',
  'root' => '/mnt/gfs/alaskadispatch.dev/livedev/docroot',
);

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site alaskadispatch, environment prod
$aliases['prod'] = array(
  'root' => '/var/www/html/alaskadispatch.prod/docroot',
  'ac-site' => 'alaskadispatch',
  'ac-env' => 'prod',
  'ac-realm' => 'prod',
  'uri' => 'alaskadispatch.prod.acquia-sites.com',
  'remote-host' => 'web-3781.prod.hosting.acquia.com',
  'remote-user' => 'alaskadispatch.prod',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['prod.livedev'] = array(
  'parent' => '@alaskadispatch.prod',
  'root' => '/mnt/gfs/alaskadispatch.prod/livedev/docroot',
);

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site alaskadispatch, environment ra
$aliases['ra'] = array(
  'root' => '/var/www/html/alaskadispatch.ra/docroot',
  'ac-site' => 'alaskadispatch',
  'ac-env' => 'ra',
  'ac-realm' => 'prod',
  'uri' => 'alaskadispatchra.prod.acquia-sites.com',
  'remote-host' => 'staging-8731.prod.hosting.acquia.com',
  'remote-user' => 'alaskadispatch.ra',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['ra.livedev'] = array(
  'parent' => '@alaskadispatch.ra',
  'root' => '/mnt/gfs/alaskadispatch.ra/livedev/docroot',
);

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site alaskadispatch, environment test
$aliases['test'] = array(
  'root' => '/var/www/html/alaskadispatch.test/docroot',
  'ac-site' => 'alaskadispatch',
  'ac-env' => 'test',
  'ac-realm' => 'prod',
  'uri' => 'alaskadisstg.prod.acquia-sites.com',
  'remote-host' => 'staging-4594.prod.hosting.acquia.com',
  'remote-user' => 'alaskadispatch.test',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['test.livedev'] = array(
  'parent' => '@alaskadispatch.test',
  'root' => '/mnt/gfs/alaskadispatch.test/livedev/docroot',
);
