<?php
// Dev site.
$aliases['ct-stage'] = array(
  'root' => '/u01/www/wwe/html',
  'env' => 'dev',
  'uri' => 'www.ct-stage.cloud.wwe.com',
  'remote-host' => 'www.ct-stage.cloud.wwe.com',
  'remote-user' => 'kbasarab',
  'target-command-specific' => array(
    'sql-sync' => array(
      'no-cache' => TRUE,
      'sanitize' => FALSE,
      'simulate' => 1,
      'no-ordered-dump' => TRUE,
      'skip-tables-key' => 'common',
      'structure-tables' => array(
        'common' => array(
          'cache',
          'cache_block',
          'cache_content',
          'cache_filter',
          'cache_menu',
          'cache_page',
          'sessions',
          'watchdog'
        ),
      ),
    ),
  ),
);

$aliases['jenkins.kevin'] = array(
  'root' => '/u01/www/wwe3redesign/html',
  'env' => 'dev',
  'uri' => 'wwe-kevin.jenkins.wwe.com',
  'remote-host' => 'wwe-kevin.jenkins.wwe.com',
  'remote-user' => 'kbasarab',
  'target-command-specific' => array(
    'sql-sync' => array(
      'no-cache' => TRUE,
      'sanitize' => FALSE,
      'simulate' => 1,
      'no-ordered-dump' => TRUE,
      'skip-tables-key' => 'common',
      'structure-tables' => array(
        'common' => array(
          'cache',
          'cache_block',
          'cache_content',
          'cache_filter',
          'cache_menu',
          'cache_page',
          'sessions',
          'watchdog'
        ),
      ),
    ),
  ),
);
