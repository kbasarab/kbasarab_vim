<?php

// Site alaskad7, environment dev
$aliases['lsu.local'] = array(
  'root' => '/Users/kbasarab/www/sites/lsu.kb/docroot',
  'uri' => 'lsu.kb',
  'path-aliases' => array(
    '%files' => 'sites/default/files',
  ),
  'skip-tables' => array('common' =>
    array('users', 'users_roles', 'roles', 'permission', 'node'),
  ),
  'target-command-specific' => array(
    'sql-sync' => array(
      'confirm-sanitzations' => TRUE,
      'sanitize' => TRUE
    ),
  ),
);

