<?php

// Application 'mercycollege', environment 'prod'.
$aliases['prod'] = array (
  'root' => '/var/www/html/mercycollege.prod/docroot',
  'ac-site' => 'mercycollege',
  'ac-env' => 'prod',
  'ac-realm' => 'prod',
  'uri' => 'mercycollege.prod.acquia-sites.com',
  'path-aliases' => 
  array (
    '%drush-script' => 'drush8',
  ),
  'prod.livedev' => 
  array (
    'parent' => '@mercycollege.prod',
    'root' => '/mnt/gfs/mercycollege.prod/livedev/docroot',
  ),
  'remote-host' => 'mercycollege.ssh.prod.acquia-sites.com',
  'remote-user' => 'mercycollege.prod',
);

// Application 'mercycollege', environment 'dev'.
$aliases['dev'] = array (
  'root' => '/var/www/html/mercycollege.dev/docroot',
  'ac-site' => 'mercycollege',
  'ac-env' => 'dev',
  'ac-realm' => 'prod',
  'uri' => 'mercycollegedev.prod.acquia-sites.com',
  'path-aliases' => 
  array (
    '%drush-script' => 'drush8',
  ),
  'dev.livedev' => 
  array (
    'parent' => '@mercycollege.dev',
    'root' => '/mnt/gfs/mercycollege.dev/livedev/docroot',
  ),
  'remote-host' => 'mercycollegedev.ssh.prod.acquia-sites.com',
  'remote-user' => 'mercycollege.dev',
);

// Application 'mercycollege', environment 'test'.
$aliases['test'] = array (
  'root' => '/var/www/html/mercycollege.test/docroot',
  'ac-site' => 'mercycollege',
  'ac-env' => 'test',
  'ac-realm' => 'prod',
  'uri' => 'mercycollegestg.prod.acquia-sites.com',
  'path-aliases' => 
  array (
    '%drush-script' => 'drush8',
  ),
  'test.livedev' => 
  array (
    'parent' => '@mercycollege.test',
    'root' => '/mnt/gfs/mercycollege.test/livedev/docroot',
  ),
  'remote-host' => 'mercycollegestg.ssh.prod.acquia-sites.com',
  'remote-user' => 'mercycollege.test',
);

// Application 'mercycollege', environment 'ra'.
$aliases['ra'] = array (
  'root' => '/var/www/html/mercycollege.ra/docroot',
  'ac-site' => 'mercycollege',
  'ac-env' => 'ra',
  'ac-realm' => 'prod',
  'uri' => 'mercycollegera.prod.acquia-sites.com',
  'path-aliases' => 
  array (
    '%drush-script' => 'drush8',
  ),
  'ra.livedev' => 
  array (
    'parent' => '@mercycollege.ra',
    'root' => '/mnt/gfs/mercycollege.ra/livedev/docroot',
  ),
  'remote-host' => 'mercycollegera.ssh.prod.acquia-sites.com',
  'remote-user' => 'mercycollege.ra',
);

// Application 'mercycollege', environment 'marketing'.
$aliases['marketing'] = array (
  'root' => '/var/www/html/mercycollege.marketing/docroot',
  'ac-site' => 'mercycollege',
  'ac-env' => 'marketing',
  'ac-realm' => 'prod',
  'uri' => 'mercycollegemarketing.prod.acquia-sites.com',
  'path-aliases' => 
  array (
    '%drush-script' => 'drush8',
  ),
  'marketing.livedev' => 
  array (
    'parent' => '@mercycollege.marketing',
    'root' => '/mnt/gfs/mercycollege.marketing/livedev/docroot',
  ),
  'remote-host' => 'mercycollegemarketing.ssh.prod.acquia-sites.com',
  'remote-user' => 'mercycollege.marketing',
);

// Application 'mercycollege', environment 'automation'.
$aliases['automation'] = array (
  'root' => '/var/www/html/mercycollege.automation/docroot',
  'ac-site' => 'mercycollege',
  'ac-env' => 'automation',
  'ac-realm' => 'prod',
  'uri' => 'mercycollegeautomation.prod.acquia-sites.com',
  'path-aliases' => 
  array (
    '%drush-script' => 'drush8',
  ),
  'automation.livedev' => 
  array (
    'parent' => '@mercycollege.automation',
    'root' => '/mnt/gfs/mercycollege.automation/livedev/docroot',
  ),
  'remote-host' => 'mercycollegeautomation.ssh.prod.acquia-sites.com',
  'remote-user' => 'mercycollege.automation',
);

