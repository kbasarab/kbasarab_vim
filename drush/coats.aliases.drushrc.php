<?php
/**
*file base.aliases.drushrc.php
* Place this file in your ~./drush directory. Create the
* directory if you don't have one yet.
* $cd ~; mkdir .drush
*/

/**
 * Drush commands will be in the form of from-environment -> to-environment
 *
 * Example usage would be to sync your local copy to production.
 *
 * Use rsync to sync the files from production to local.
 * $ drush rsync @base.prod:%files @base.local:%files
 *
 * Use sql-sync to copy the DB from production to dev.
 * $ drush sql-sync --no-cache @base.prod @base.dev
 *
 * Want to copy DB from production to staging?
 * $ drush sql-sync --no-cache @base.prod @base.local
 *
 * Because %dump-dir is defined for local a copy will be left there.
 * You need "--no-cache" unless you have command-specific sql-sync --no-cache
 * defined because otherwise drush will pull the latest dump made in the last
 * 24 hours which obviously may be out of date.
 *
 * After adding the command-specific --no-cache you can just use:
 * $ drush sql-sync @base.dev @base.local
 */

// Site environment dev
$aliases['coats.dev'] = array(
    'root' => '/vhosts/dev.knitrowan.com/httpdocs',
    'remote-host' => 'dev.knitrowan.com',
    'remote-user' => 'mmarano',
    'uri' => 'dev.knitrowan.com',
    'path-aliases' => array(
      '%files' => 'files',
      '%dump-dir' => '/tmp',
      ),
    'source-command-specific' => array (
      'sql-sync' => array (
        'no-cache' => TRUE,
        'structure-tables-key' => 'common',
        ),
      ),
    'command-specific' => array (
      'sql-sync' => array (
        'sanitize' => TRUE,
        'no-ordered-dump' => TRUE,
        'structure-tables' => array( // Add tables that should have data skipped during sql-sync
          'common' => array('cache', 'cache_block', 'cache_content', 'cache_filter', 'cache_menu', 'cache_page', 'sessions', 'watchdog'),
          ),
        ),
      ),

    'command-specific' => array (
        'sql-sync' => array (
          'no-cache' => TRUE, // keep drush from serving a cached db dump.
          ),
        ),
    );

// Site environment production
$aliases['coats.prod'] = array(
    'parent' => '@coats.dev',
    'remote-host' => 'http://knitrowan.com',
    'root' => '/vhosts/w1.knitrowan.com/httpdocs',
    'uri' => 'redheart.com',
    'target-command-specific' => array ( // Adds some protection against accidental overwrites.
      'sql-sync' => array (
        'simulate' => '1', // Prevent sql-sync to prod.
        ),
      'rsync' => array (
        'simulate' => '1', // Prevent rsync to prod.
        ),
      ),
    );

// Site environment test/stage
$aliases['coats.stage'] = array(
    'remote-host' => 'stg.knitrowan.com',
    'parent' => '@coats.dev',
    'root' => '/vhosts/stg.knitrowan.com/httpdocs',
    'uri' => 'stg.knitrowan.com',
    );

// Site environment local
$aliases['coats.local'] = array(
    'root' => '/Users/kbasarab/www/sites/coats.kb',
    'uri' => 'coats.kb',
    'path-aliases' => array(
      '%files' => 'files',
      // Path for sql-sync dumps.
      //'%dump' => '/Volumes/Sites/drush-dumps/test_dump-' . date('Ymd-His') . '.sql', // create dated DB dumps.
      ),
    );;
