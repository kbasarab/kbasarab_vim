<?php
/**
*file base.aliases.drushrc.php
* Place this file in your ~./drush directory. Create the
* directory if you don't have one yet.
* $cd ~; mkdir .drush
*/

/**
 * Drush commands will be in the form of from-environment -> to-environment
 *
 * Example usage would be to sync your local copy to production.
 *
 * Use rsync to sync the files from production to local.
 * $ drush rsync @base.prod:%files @base.local:%files
 *
 * Use sql-sync to copy the DB from production to dev.
 * $ drush sql-sync --no-cache @base.prod @base.dev
 *
 * Want to copy DB from production to staging?
 * $ drush sql-sync --no-cache @base.prod @base.local
 *
 * Because %dump-dir is defined for local a copy will be left there.
 * You need "--no-cache" unless you have command-specific sql-sync --no-cache
 * defined because otherwise drush will pull the latest dump made in the last
 * 24 hours which obviously may be out of date.
 *
 * After adding the command-specific --no-cache you can just use:
 * $ drush sql-sync @base.dev @base.local
 */
$aliases['gleaner.local'] = array(
    'root' => '/Volumes/500/www/gleaner.d7',
    'uri' => 'gleaner.d7',
    'path-aliases' => array(
      '%files' => 'sites/default/files',
      // Path for sql-sync dumps.
      ),
    );;
