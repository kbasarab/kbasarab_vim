<?php

$aliases['d8.local'] = array(
   'uri' => 'd8.local',
   'remote-host' => 'd8.local',
   'remote-user' => 'vagrant',
   'ssh-options' => '-i /Users/kbasarab/.vagrant.d/insecure_private_key -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no',
   'root' => '/var/beetbox/docroot',
);
