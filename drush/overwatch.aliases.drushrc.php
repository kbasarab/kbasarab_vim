<?php

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site overwatch, environment dev.
$aliases['dev'] = array(
  'root' => '/var/www/html/overwatch.dev/docroot',
  'ac-site' => 'overwatch',
  'ac-env' => 'dev',
  'ac-realm' => 'prod',
  'uri' => 'overwatchdev.prod.acquia-sites.com',
  'remote-host' => 'overwatchdev.ssh.prod.acquia-sites.com',
  'remote-user' => 'overwatch.dev',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  ),
);
$aliases['dev.livedev'] = array(
  'parent' => '@overwatch.dev',
  'root' => '/mnt/gfs/overwatch.dev/livedev/docroot',
);

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site overwatch, environment prod.
$aliases['prod'] = array(
  'root' => '/var/www/html/overwatch.prod/docroot',
  'ac-site' => 'overwatch',
  'ac-env' => 'prod',
  'ac-realm' => 'prod',
  'uri' => 'overwatch.prod.acquia-sites.com',
  'remote-host' => 'overwatch.ssh.prod.acquia-sites.com',
  'remote-user' => 'overwatch.prod',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  ),
);
$aliases['prod.livedev'] = array(
  'parent' => '@overwatch.prod',
  'root' => '/mnt/gfs/overwatch.prod/livedev/docroot',
);

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site overwatch, environment ra.
$aliases['ra'] = array(
  'root' => '/var/www/html/overwatch.ra/docroot',
  'ac-site' => 'overwatch',
  'ac-env' => 'ra',
  'ac-realm' => 'prod',
  'uri' => 'overwatchra.prod.acquia-sites.com',
  'remote-host' => 'overwatchra.ssh.prod.acquia-sites.com',
  'remote-user' => 'overwatch.ra',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  ),
);
$aliases['ra.livedev'] = array(
  'parent' => '@overwatch.ra',
  'root' => '/mnt/gfs/overwatch.ra/livedev/docroot',
);

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site overwatch, environment test.
$aliases['test'] = array(
  'root' => '/var/www/html/overwatch.test/docroot',
  'ac-site' => 'overwatch',
  'ac-env' => 'test',
  'ac-realm' => 'prod',
  'uri' => 'overwatchstg.prod.acquia-sites.com',
  'remote-host' => 'overwatchstg.ssh.prod.acquia-sites.com',
  'remote-user' => 'overwatch.test',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  ),
);
$aliases['test.livedev'] = array(
  'parent' => '@overwatch.test',
  'root' => '/mnt/gfs/overwatch.test/livedev/docroot',
);
