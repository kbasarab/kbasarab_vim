<?php

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site domesticweather, environment c32xlarge
$aliases['c32xlarge'] = array(
  'root' => '/var/www/html/domesticweather.c32xlarge/docroot',
  'ac-site' => 'domesticweather',
  'ac-env' => 'c32xlarge',
  'ac-realm' => 'prod',
  'uri' => 'domesticweatherc32xlarge.prod.acquia-sites.com',
  'remote-host' => 'staging-10506.prod.hosting.acquia.com',
  'remote-user' => 'domesticweather.c32xlarge',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['c32xlarge.livedev'] = array(
  'parent' => '@domesticweather.c32xlarge',
  'root' => '/mnt/gfs/domesticweather.c32xlarge/livedev/docroot',
);

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site domesticweather, environment c34xlarge
$aliases['c34xlarge'] = array(
  'root' => '/var/www/html/domesticweather.c34xlarge/docroot',
  'ac-site' => 'domesticweather',
  'ac-env' => 'c34xlarge',
  'ac-realm' => 'prod',
  'uri' => 'domesticweatherc34xlarge.prod.acquia-sites.com',
  'remote-host' => 'staging-10507.prod.hosting.acquia.com',
  'remote-user' => 'domesticweather.c34xlarge',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['c34xlarge.livedev'] = array(
  'parent' => '@domesticweather.c34xlarge',
  'root' => '/mnt/gfs/domesticweather.c34xlarge/livedev/docroot',
);

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site domesticweather, environment dev
$aliases['dev'] = array(
  'root' => '/var/www/html/domesticweather.dev/docroot',
  'ac-site' => 'domesticweather',
  'ac-env' => 'dev',
  'ac-realm' => 'prod',
  'uri' => 'domesticweatherdev.prod.acquia-sites.com',
  'remote-host' => 'staging-9435.prod.hosting.acquia.com',
  'remote-user' => 'domesticweather.dev',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['dev.livedev'] = array(
  'parent' => '@domesticweather.dev',
  'root' => '/mnt/gfs/domesticweather.dev/livedev/docroot',
);

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site domesticweather, environment m3xlarge
$aliases['m3xlarge'] = array(
  'root' => '/var/www/html/domesticweather.m3xlarge/docroot',
  'ac-site' => 'domesticweather',
  'ac-env' => 'm3xlarge',
  'ac-realm' => 'prod',
  'uri' => 'domesticweatherm3xlarge.prod.acquia-sites.com',
  'remote-host' => 'staging-10508.prod.hosting.acquia.com',
  'remote-user' => 'domesticweather.m3xlarge',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['m3xlarge.livedev'] = array(
  'parent' => '@domesticweather.m3xlarge',
  'root' => '/mnt/gfs/domesticweather.m3xlarge/livedev/docroot',
);

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site domesticweather, environment prod
$aliases['prod'] = array(
  'root' => '/var/www/html/domesticweather.prod/docroot',
  'ac-site' => 'domesticweather',
  'ac-env' => 'prod',
  'ac-realm' => 'prod',
  'uri' => 'domesticweather.prod.acquia-sites.com',
  'remote-host' => 'web-9436.prod.hosting.acquia.com',
  'remote-user' => 'domesticweather.prod',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['prod.livedev'] = array(
  'parent' => '@domesticweather.prod',
  'root' => '/mnt/gfs/domesticweather.prod/livedev/docroot',
);

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site domesticweather, environment ra
$aliases['ra'] = array(
  'root' => '/var/www/html/domesticweather.ra/docroot',
  'ac-site' => 'domesticweather',
  'ac-env' => 'ra',
  'ac-realm' => 'prod',
  'uri' => 'domesticweatherra.prod.acquia-sites.com',
  'remote-host' => 'staging-8880.prod.hosting.acquia.com',
  'remote-user' => 'domesticweather.ra',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['ra.livedev'] = array(
  'parent' => '@domesticweather.ra',
  'root' => '/mnt/gfs/domesticweather.ra/livedev/docroot',
);

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site domesticweather, environment test
$aliases['test'] = array(
  'root' => '/var/www/html/domesticweather.test/docroot',
  'ac-site' => 'domesticweather',
  'ac-env' => 'test',
  'ac-realm' => 'prod',
  'uri' => 'domesticweathertest.prod.acquia-sites.com',
  'remote-host' => 'staging-9435.prod.hosting.acquia.com',
  'remote-user' => 'domesticweather.test',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['test.livedev'] = array(
  'parent' => '@domesticweather.test',
  'root' => '/mnt/gfs/domesticweather.test/livedev/docroot',
);
