<?php
  /**
   * Pantheon drush alias file, to be placed in your ~/.drush directory or the aliases
   * directory of your local Drush home. Once it's in place, clear drush cache:
   *
   * drush cc drush
   *
   * To see all your available aliases:
   *
   * drush sa
   *
   * See http://helpdesk.getpantheon.com/customer/portal/articles/411388 for details.
   */

  $aliases['ny-senate.dev'] = array(
    'uri' => 'dev-ny-senate.pantheonsite.io',
    'db-url' => 'mysql://pantheon:121c77b207c740a2afa64029dbe0760a@dbserver.dev.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9.drush.in:10902/pantheon',
    'db-allows-remote' => TRUE,
    'remote-host' => 'appserver.dev.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9.drush.in',
    'remote-user' => 'dev.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9',
    'ssh-options' => '-p 2222 -o "AddressFamily inet"',
    'path-aliases' => array(
      '%files' => 'code/sites/default/files',
      '%drush-script' => 'drush',
     ),
  );
  $aliases['ny-senate.alison'] = array(
    'uri' => 'alison-ny-senate.pantheonsite.io',
    'db-url' => 'mysql://pantheon:4df35a8f2e154ba6926f16c8b146c630@dbserver.alison.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9.drush.in:21610/pantheon',
    'db-allows-remote' => TRUE,
    'remote-host' => 'appserver.alison.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9.drush.in',
    'remote-user' => 'alison.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9',
    'ssh-options' => '-p 2222 -o "AddressFamily inet"',
    'path-aliases' => array(
      '%files' => 'code/sites/default/files',
      '%drush-script' => 'drush',
     ),
  );
  $aliases['ny-senate.dh-training'] = array(
    'uri' => 'dh-training-ny-senate.pantheonsite.io',
    'db-url' => 'mysql://pantheon:ebe55c27beb44bad895e934cb5fa0e33@dbserver.dh-training.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9.drush.in:29457/pantheon',
    'db-allows-remote' => TRUE,
    'remote-host' => 'appserver.dh-training.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9.drush.in',
    'remote-user' => 'dh-training.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9',
    'ssh-options' => '-p 2222 -o "AddressFamily inet"',
    'path-aliases' => array(
      '%files' => 'code/sites/default/files',
      '%drush-script' => 'drush',
     ),
  );
  $aliases['ny-senate.sbb3'] = array(
    'uri' => 'sbb3-ny-senate.pantheonsite.io',
    'db-url' => 'mysql://pantheon:b4fefa52cb314385a7b60fcd90bd6def@dbserver.sbb3.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9.drush.in:10995/pantheon',
    'db-allows-remote' => TRUE,
    'remote-host' => 'appserver.sbb3.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9.drush.in',
    'remote-user' => 'sbb3.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9',
    'ssh-options' => '-p 2222 -o "AddressFamily inet"',
    'path-aliases' => array(
      '%files' => 'code/sites/default/files',
      '%drush-script' => 'drush',
     ),
  );
  $aliases['ny-senate.rtb'] = array(
    'uri' => 'rtb-ny-senate.pantheonsite.io',
    'db-url' => 'mysql://pantheon:984fed9fe5234dc3ab012aebc820dbca@dbserver.rtb.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9.drush.in:10937/pantheon',
    'db-allows-remote' => TRUE,
    'remote-host' => 'appserver.rtb.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9.drush.in',
    'remote-user' => 'rtb.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9',
    'ssh-options' => '-p 2222 -o "AddressFamily inet"',
    'path-aliases' => array(
      '%files' => 'code/sites/default/files',
      '%drush-script' => 'drush',
     ),
  );
  $aliases['ny-senate.pantheoncse'] = array(
    'uri' => 'pantheoncse-ny-senate.pantheonsite.io',
    'db-url' => 'mysql://pantheon:a5f3dca278ee491fb58e81d46fb36836@dbserver.pantheoncse.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9.drush.in:10984/pantheon',
    'db-allows-remote' => TRUE,
    'remote-host' => 'appserver.pantheoncse.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9.drush.in',
    'remote-user' => 'pantheoncse.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9',
    'ssh-options' => '-p 2222 -o "AddressFamily inet"',
    'path-aliases' => array(
      '%files' => 'code/sites/default/files',
      '%drush-script' => 'drush',
     ),
  );
  $aliases['ny-senate.live'] = array(
    'uri' => 'senate.ny.gov',
    'db-url' => 'mysql://pantheon:aae588cca14c48728d44e3495db94734@dbserver.live.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9.drush.in:11563/pantheon',
    'db-allows-remote' => TRUE,
    'remote-host' => 'appserver.live.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9.drush.in',
    'remote-user' => 'live.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9',
    'ssh-options' => '-p 2222 -o "AddressFamily inet"',
    'path-aliases' => array(
      '%files' => 'code/sites/default/files',
      '%drush-script' => 'drush',
     ),
  );
  $aliases['ny-senate.integration'] = array(
    'uri' => 'integration-ny-senate.pantheonsite.io',
    'db-url' => 'mysql://pantheon:0dbcb56ee0e349faa60117f508c42e68@dbserver.integration.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9.drush.in:11012/pantheon',
    'db-allows-remote' => TRUE,
    'remote-host' => 'appserver.integration.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9.drush.in',
    'remote-user' => 'integration.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9',
    'ssh-options' => '-p 2222 -o "AddressFamily inet"',
    'path-aliases' => array(
      '%files' => 'code/sites/default/files',
      '%drush-script' => 'drush',
     ),
  );
  $aliases['ny-senate.training'] = array(
    'uri' => 'training-ny-senate.pantheonsite.io',
    'db-url' => 'mysql://pantheon:dd8958d84eb443389ba93e1e9fde106b@dbserver.training.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9.drush.in:10989/pantheon',
    'db-allows-remote' => TRUE,
    'remote-host' => 'appserver.training.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9.drush.in',
    'remote-user' => 'training.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9',
    'ssh-options' => '-p 2222 -o "AddressFamily inet"',
    'path-aliases' => array(
      '%files' => 'code/sites/default/files',
      '%drush-script' => 'drush',
     ),
  );
  $aliases['ny-senate.test'] = array(
    'uri' => 'test-ny-senate.pantheon.io',
    'db-url' => 'mysql://pantheon:943125c8341d44ecac1c12320a84fe75@dbserver.test.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9.drush.in:23021/pantheon',
    'db-allows-remote' => TRUE,
    'remote-host' => 'appserver.test.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9.drush.in',
    'remote-user' => 'test.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9',
    'ssh-options' => '-p 2222 -o "AddressFamily inet"',
    'path-aliases' => array(
      '%files' => 'code/sites/default/files',
      '%drush-script' => 'drush',
     ),
  );
  $aliases['ny-senate.seth'] = array(
    'uri' => 'seth-ny-senate.pantheonsite.io',
    'db-url' => 'mysql://pantheon:29622623f91a4002a9c90946036410db@dbserver.seth.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9.drush.in:10665/pantheon',
    'db-allows-remote' => TRUE,
    'remote-host' => 'appserver.seth.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9.drush.in',
    'remote-user' => 'seth.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9',
    'ssh-options' => '-p 2222 -o "AddressFamily inet"',
    'path-aliases' => array(
      '%files' => 'code/sites/default/files',
      '%drush-script' => 'drush',
     ),
  );
  $aliases['ny-senate.kevinfc'] = array(
    'uri' => 'kevinfc-ny-senate.pantheonsite.io',
    'db-url' => 'mysql://pantheon:85b260c6d5a34cdabaed8bb22788226c@dbserver.kevinfc.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9.drush.in:11313/pantheon',
    'db-allows-remote' => TRUE,
    'remote-host' => 'appserver.kevinfc.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9.drush.in',
    'remote-user' => 'kevinfc.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9',
    'ssh-options' => '-p 2222 -o "AddressFamily inet"',
    'path-aliases' => array(
      '%files' => 'code/sites/default/files',
      '%drush-script' => 'drush',
     ),
  );
  $aliases['ny-senate.slk2'] = array(
    'uri' => 'slk2-ny-senate.pantheonsite.io',
    'db-url' => 'mysql://pantheon:8e9c3788143c4f7db856d6b6a81230a7@dbserver.slk2.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9.drush.in:10498/pantheon',
    'db-allows-remote' => TRUE,
    'remote-host' => 'appserver.slk2.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9.drush.in',
    'remote-user' => 'slk2.4c1822eb-3e98-45ae-bbfd-4ea751b4b5a9',
    'ssh-options' => '-p 2222 -o "AddressFamily inet"',
    'path-aliases' => array(
      '%files' => 'code/sites/default/files',
      '%drush-script' => 'drush',
     ),
  );
  $aliases['mc-thrillist.live'] = array(
    'uri' => 'live-mc-thrillist.pantheonsite.io',
    'db-url' => 'mysql://pantheon:e56bf3b3f88744e3aedb2af1dbb79611@dbserver.live.23300156-41ce-4547-a064-6d689c800d98.drush.in:10593/pantheon',
    'db-allows-remote' => TRUE,
    'remote-host' => 'appserver.live.23300156-41ce-4547-a064-6d689c800d98.drush.in',
    'remote-user' => 'live.23300156-41ce-4547-a064-6d689c800d98',
    'ssh-options' => '-p 2222 -o "AddressFamily inet"',
    'path-aliases' => array(
      '%files' => 'code/sites/default/files',
      '%drush-script' => 'drush',
     ),
  );
  $aliases['mc-thrillist.test'] = array(
    'uri' => 'test-mc-thrillist.pantheonsite.io',
    'db-url' => 'mysql://pantheon:d6e37e609ce446729444b38145231068@dbserver.test.23300156-41ce-4547-a064-6d689c800d98.drush.in:10946/pantheon',
    'db-allows-remote' => TRUE,
    'remote-host' => 'appserver.test.23300156-41ce-4547-a064-6d689c800d98.drush.in',
    'remote-user' => 'test.23300156-41ce-4547-a064-6d689c800d98',
    'ssh-options' => '-p 2222 -o "AddressFamily inet"',
    'path-aliases' => array(
      '%files' => 'code/sites/default/files',
      '%drush-script' => 'drush',
     ),
  );
  $aliases['mc-thrillist.develop'] = array(
    'uri' => 'develop-mc-thrillist.pantheonsite.io',
    'db-url' => 'mysql://pantheon:10f2a3e5b73b4bada8e31e2a24b0e1d4@dbserver.develop.23300156-41ce-4547-a064-6d689c800d98.drush.in:11008/pantheon',
    'db-allows-remote' => TRUE,
    'remote-host' => 'appserver.develop.23300156-41ce-4547-a064-6d689c800d98.drush.in',
    'remote-user' => 'develop.23300156-41ce-4547-a064-6d689c800d98',
    'ssh-options' => '-p 2222 -o "AddressFamily inet"',
    'path-aliases' => array(
      '%files' => 'code/sites/default/files',
      '%drush-script' => 'drush',
     ),
  );
  $aliases['mc-thrillist.dev'] = array(
    'uri' => 'dev-mc-thrillist.pantheonsite.io',
    'db-url' => 'mysql://pantheon:195acf7729434b9495f5b23a15d9bc7b@dbserver.dev.23300156-41ce-4547-a064-6d689c800d98.drush.in:11002/pantheon',
    'db-allows-remote' => TRUE,
    'remote-host' => 'appserver.dev.23300156-41ce-4547-a064-6d689c800d98.drush.in',
    'remote-user' => 'dev.23300156-41ce-4547-a064-6d689c800d98',
    'ssh-options' => '-p 2222 -o "AddressFamily inet"',
    'path-aliases' => array(
      '%files' => 'code/sites/default/files',
      '%drush-script' => 'drush',
     ),
  );
  $aliases['mediacurrent-demo.cohnreznick'] = array(
    'uri' => 'cohnreznick-mediacurrent-demo.pantheonsite.io',
    'db-url' => 'mysql://pantheon:f64a5c3c6f1545799600d1f93b761a1b@dbserver.cohnreznick.728d1922-10ea-44a4-8afb-a185e1dbbef6.drush.in:11719/pantheon',
    'db-allows-remote' => TRUE,
    'remote-host' => 'appserver.cohnreznick.728d1922-10ea-44a4-8afb-a185e1dbbef6.drush.in',
    'remote-user' => 'cohnreznick.728d1922-10ea-44a4-8afb-a185e1dbbef6',
    'ssh-options' => '-p 2222 -o "AddressFamily inet"',
    'path-aliases' => array(
      '%files' => 'code/sites/default/files',
      '%drush-script' => 'drush',
     ),
  );
  $aliases['mediacurrent-demo.mit-sloan'] = array(
    'uri' => 'mit-sloan-mediacurrent-demo.pantheonsite.io',
    'db-url' => 'mysql://pantheon:9db334c8717142888b2a5d79d9612e07@dbserver.mit-sloan.728d1922-10ea-44a4-8afb-a185e1dbbef6.drush.in:26179/pantheon',
    'db-allows-remote' => TRUE,
    'remote-host' => 'appserver.mit-sloan.728d1922-10ea-44a4-8afb-a185e1dbbef6.drush.in',
    'remote-user' => 'mit-sloan.728d1922-10ea-44a4-8afb-a185e1dbbef6',
    'ssh-options' => '-p 2222 -o "AddressFamily inet"',
    'path-aliases' => array(
      '%files' => 'code/sites/default/files',
      '%drush-script' => 'drush',
     ),
  );
  $aliases['mediacurrent-demo.care-org'] = array(
    'uri' => 'care-org-mediacurrent-demo.pantheonsite.io',
    'db-url' => 'mysql://pantheon:115160d8f461414291e6b3bff81771c4@dbserver.care-org.728d1922-10ea-44a4-8afb-a185e1dbbef6.drush.in:24375/pantheon',
    'db-allows-remote' => TRUE,
    'remote-host' => 'appserver.care-org.728d1922-10ea-44a4-8afb-a185e1dbbef6.drush.in',
    'remote-user' => 'care-org.728d1922-10ea-44a4-8afb-a185e1dbbef6',
    'ssh-options' => '-p 2222 -o "AddressFamily inet"',
    'path-aliases' => array(
      '%files' => 'code/sites/default/files',
      '%drush-script' => 'drush',
     ),
  );
  $aliases['mediacurrent-demo.test'] = array(
    'uri' => 'test-mediacurrent-demo.pantheonsite.io',
    'db-url' => 'mysql://pantheon:406a760f763045e1a80df160cb7fa1fa@dbserver.test.728d1922-10ea-44a4-8afb-a185e1dbbef6.drush.in:24166/pantheon',
    'db-allows-remote' => TRUE,
    'remote-host' => 'appserver.test.728d1922-10ea-44a4-8afb-a185e1dbbef6.drush.in',
    'remote-user' => 'test.728d1922-10ea-44a4-8afb-a185e1dbbef6',
    'ssh-options' => '-p 2222 -o "AddressFamily inet"',
    'path-aliases' => array(
      '%files' => 'code/sites/default/files',
      '%drush-script' => 'drush',
     ),
  );
  $aliases['mediacurrent-demo.dev'] = array(
    'uri' => 'dev-mediacurrent-demo.pantheonsite.io',
    'db-url' => 'mysql://pantheon:b2eec45f24d743e69c2aeea6304c2909@dbserver.dev.728d1922-10ea-44a4-8afb-a185e1dbbef6.drush.in:29498/pantheon',
    'db-allows-remote' => TRUE,
    'remote-host' => 'appserver.dev.728d1922-10ea-44a4-8afb-a185e1dbbef6.drush.in',
    'remote-user' => 'dev.728d1922-10ea-44a4-8afb-a185e1dbbef6',
    'ssh-options' => '-p 2222 -o "AddressFamily inet"',
    'path-aliases' => array(
      '%files' => 'code/sites/default/files',
      '%drush-script' => 'drush',
     ),
  );
  $aliases['mediacurrent-demo.live'] = array(
    'uri' => 'live-mediacurrent-demo.pantheonsite.io',
    'db-url' => 'mysql://pantheon:501d0540d71748a380a740992212d8ef@dbserver.live.728d1922-10ea-44a4-8afb-a185e1dbbef6.drush.in:24553/pantheon',
    'db-allows-remote' => TRUE,
    'remote-host' => 'appserver.live.728d1922-10ea-44a4-8afb-a185e1dbbef6.drush.in',
    'remote-user' => 'live.728d1922-10ea-44a4-8afb-a185e1dbbef6',
    'ssh-options' => '-p 2222 -o "AddressFamily inet"',
    'path-aliases' => array(
      '%files' => 'code/sites/default/files',
      '%drush-script' => 'drush',
     ),
  );
  $aliases['mediacurrent-demo.develop'] = array(
    'uri' => 'develop-mediacurrent-demo.pantheonsite.io',
    'db-url' => 'mysql://pantheon:27066f84e80a4b948baf6eae86f637ce@dbserver.develop.728d1922-10ea-44a4-8afb-a185e1dbbef6.drush.in:20871/pantheon',
    'db-allows-remote' => TRUE,
    'remote-host' => 'appserver.develop.728d1922-10ea-44a4-8afb-a185e1dbbef6.drush.in',
    'remote-user' => 'develop.728d1922-10ea-44a4-8afb-a185e1dbbef6',
    'ssh-options' => '-p 2222 -o "AddressFamily inet"',
    'path-aliases' => array(
      '%files' => 'code/sites/default/files',
      '%drush-script' => 'drush',
     ),
  );
  $aliases['mediacurrent-demo.twcsandbox'] = array(
    'uri' => 'twcsandbox-mediacurrent-demo.pantheonsite.io',
    'db-url' => 'mysql://pantheon:5a5f5944efab4079a710b1dcc9e14e1b@dbserver.twcsandbox.728d1922-10ea-44a4-8afb-a185e1dbbef6.drush.in:22877/pantheon',
    'db-allows-remote' => TRUE,
    'remote-host' => 'appserver.twcsandbox.728d1922-10ea-44a4-8afb-a185e1dbbef6.drush.in',
    'remote-user' => 'twcsandbox.728d1922-10ea-44a4-8afb-a185e1dbbef6',
    'ssh-options' => '-p 2222 -o "AddressFamily inet"',
    'path-aliases' => array(
      '%files' => 'code/sites/default/files',
      '%drush-script' => 'drush',
     ),
  );
