<?php
/**
*file base.aliases.drushrc.php
* Place this file in your ~./drush directory. Create the
* directory if you don't have one yet.
* $cd ~; mkdir .drush
*/

/**
 * Drush commands will be in the form of from-environment -> to-environment
 *
 * Example usage would be to sync your local copy to production.
 *
 * Use rsync to sync the files from production to local.
 * $ drush rsync @base.prod:%files @base.local:%files
 *
 * Use sql-sync to copy the DB from production to dev.
 * $ drush sql-sync --no-cache @base.prod @base.dev
 *
 * Want to copy DB from production to staging?
 * $ drush sql-sync --no-cache @base.prod @base.local
 *
 * Because %dump-dir is defined for local a copy will be left there.
 * You need "--no-cache" unless you have command-specific sql-sync --no-cache
 * defined because otherwise drush will pull the latest dump made in the last
 * 24 hours which obviously may be out of date.
 *
 * After adding the command-specific --no-cache you can just use:
 * $ drush sql-sync @base.dev @base.local
 */

// Site environment dev
$aliases['demo2.production'] = array(
    'root' => '/www/docroot/',
    'remote-host' => 'sitedomain.com',
    'remote-user' => 'user',
    'ssh-options' => ' -p 22',
    'uri' => 'sitedomain.com/',
    'path-aliases' => array(
      '%files' => 'sites/default/files',
      '%dump-dir' => '/tmp',
      ),
    'source-command-specific' => array (
      'sql-sync' => array (
        'no-cache' => TRUE,
        'structure-tables-key' => 'common',
        ),
      ),
    'command-specific' => array (
      'sql-sync' => array (
        'sanitize' => TRUE,
        'no-ordered-dump' => TRUE,
        'structure-tables' => array( // Add tables that should have data skipped during sql-sync
          'common' => array('cache', 'cache_block', 'cache_content', 'cache_filter', 'cache_menu', 'cache_page', 'sessions', 'watchdog'),
          ),
        ),
      ),

    'command-specific' => array (
        'sql-sync' => array (
          'no-cache' => TRUE, // keep drush from serving a cached db dump.
          ),
        ),
    );

// Site environment local
$aliases['drush1.local'] = array(
    'root' => '/Users/kbasarab/www/sites/drush-demo.kb',
    'uri' => 'drush-demo.kb',
    'path-aliases' => array(
      '%files' => 'sites/default/files',
      // Path for sql-sync dumps.
      '%dump' => '/Users/kbasarab/www/drush-dumps/test_dump-' . date('Ymd-His') . '.sql', // create dated DB dumps.
      ),
    );;


// Site environment local
$aliases['drush2.local'] = array(
    'root' => '/Users/kbasarab/www/sites/drush-demo2.kb',
    'uri' => 'drush-demo2.kb',
    'path-aliases' => array(
      '%files' => 'sites/default/files',
      // Path for sql-sync dumps.
      '%dump' => '/Users/kbasarab/www/drush-dumps/test_dump-' . date('Ymd-His') . '.sql', // create dated DB dumps.
      ),
    );;
