<?php

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site fhevents, environment dev
$aliases['dev'] = array(
  'root' => '/var/www/html/fhevents.dev/docroot',
  'ac-site' => 'fhevents',
  'ac-env' => 'dev',
  'ac-realm' => 'devcloud',
  'uri' => 'fheventsvzddy4yc4e.devcloud.acquia-sites.com',
  'remote-host' => 'free-3979.devcloud.hosting.acquia.com',
  'remote-user' => 'fhevents.dev',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['dev.livedev'] = array(
  'parent' => '@fhevents.dev',
  'root' => '/mnt/gfs/fhevents.dev/livedev/docroot',
);

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site fhevents, environment test
$aliases['test'] = array(
  'root' => '/var/www/html/fhevents.test/docroot',
  'ac-site' => 'fhevents',
  'ac-env' => 'test',
  'ac-realm' => 'devcloud',
  'uri' => 'fheventscbktdkxadq.devcloud.acquia-sites.com',
  'remote-host' => 'free-3979.devcloud.hosting.acquia.com',
  'remote-user' => 'fhevents.test',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['test.livedev'] = array(
  'parent' => '@fhevents.test',
  'root' => '/mnt/gfs/fhevents.test/livedev/docroot',
);
