<?php
/**
 * Place this file in your ~./drush directory. Create it
 * if you don't have one yet.
 * $cd ~; mkdir .drush
 */

/**
 * Drush commands will be in the form of from-environment -> to-environment
 *
 * Example usage to sync your local copy.
 • Use rsync to sync files from dev to local.
 * $ drush rsync @la.dev:%files @la.local:%files
 *
 * Want to copy DB from dev to local?
 * $ drush sql-sync --no-cache @la.dev @la.local
 * $ drush sql-sync --no-cache @la.dev @la.stage
 *
 * Because %dump-dir is defined for local a copy will be left there.
 * You need "--no-cache" unless you have command-specific sql-synch no-cache
 * defined because otherwise it will pull the latest dump made in the last
 * 24 hours which obviously may be out of date.
 *
 * After adding the command-specific no-cache you can just use:
 * $ drush sql-sync @la.dev @la.local
 */

// Site lifeally2012, environment dev
$aliases['la.dev'] = array(
  'parent' => '@la.dev',
  'root' => '/var/www/html/lifeally2012dev/docroot',
  'remote-host' => 'srv-990.devcloud.hosting.acquia.com',
  'remote-user' => 'lifeally2012',
  'uri' => 'dev.lifeally.com',
  'path-aliases' => array(
    '%files' => 'sites/default/files',
    '%dump-dir' => '/tmp',
  ),
  'command-specific' => array (
    'sql-sync' => array (
      'no-cache' => TRUE,
      // '--no-ordered-dump' => TRUE,
    ),
  ),
);

// Site lifeally2012, environment prod
$aliases['la.prod'] = array(
  'parent' => '@la.dev',
  'root' => '/var/www/html/lifeally2012/docroot',
  'uri' => 'prod.lifeally.com',
  'target-command-specific' => array (
    'sql-sync' => array (
      'simulate' => '1',
    ),
    'rsync' => array (
      'simulate' => '1',
    ),
  ),
);

// Site lifeally2012, environment test/stage
$aliases['la.stage'] = array(
  'parent' => '@la.dev',
  'root' => '/var/www/html/lifeally2012test/docroot',
  'uri' => 'staging.lifeally.com',
);

// Site lifeally2012, environment local
$aliases['la.local'] = array(
  'root' => '/Users/kbasarab/www/sites/lifeally.kb',
  'uri' => 'lifeally.kb',
  'path-aliases' => array(
    '%files' => 'sites/default/files',
     // Path for sql-sync dumps.
    #'%dump' => '/Volumes/DevSites/drush-dumps/lifeally_dev-' . date('Ymd-His') . '.sql',
  ),
);
