#/bin/sh

ln -s ~/resources/.bashrc ~/.bashrc
ln -s ~/resources/drush/.drush ~/.drush
ln -s ~/resources/.drushrc.php ~/.drushrc.php
ln -s ~/resources/tmux.conf ~/.tmux.conf
ln -s ~/resources/.tmuxinator ~/.tmuxinator
ln -s ~/resources/.gitconfig ~/.gitconfig
ln -s ~/resources/.gitk ~/.gitk
ln -s ~/resources/.gitignore_global ~/.gitignore_global
ln -s ~/resources/.vim ~/.vim
ln -s ~/resources/.vimrc ~/.vimrc
ln -s ~/resources/.zshrc ~/.zshrc

