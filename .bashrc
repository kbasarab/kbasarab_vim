alias gitinfo='~/resources/bash/git-info.sh'
alias avd='/Users/kbasarab/www/android/./tools/android avd'
alias ls='ls -G'
alias combine_pdf='ls *.pdf | sort | xargs -J % pdftk % cat output all.pdf';
alias saucelabs="java -jar /Users/kbasarab/www/Sauce-Connect-latest/Sauce-Connect.jar mediacurrent 69902a57-5f55-4021-88e8-53d7923e39f4"
alias drush7="/Users/kbasarab/resources/drush7/drush"
alias subl="/Applications/Sublime\ Text.app/Contents/SharedSupport/bin/subl"
alias drupalcs="phpcs --standard=Drupal --extensions='php,module,inc,install,test,profile,theme,js,css,info,txt'";

mcComponent() {
  npx -p yo -p generator-mc-d8-theme -c "yo mc-d8-theme:component $1"
}


function wtitle { 
  printf "\033]0;%s\007" "$1"
}

source ~/resources/tmuxinator.bash

function parse_git_branch_and_add_brackets {
  git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\ \[\1\]/'
}

export NVM_DIR=~/.nvm
source $(brew --prefix nvm)/nvm.sh

export PATH=$(brew --prefix homebrew/php/php71)/bin:/opt/local/bin:/opt/local/sbin:/usr/share:$PATH:/Users/kbasarab/resources/bin:$HOME/.composer/vendor/bin
source ~/resources/bash/git_completion.sh
#source ~/resources/bash/git-flow-completion.sh
#
#function parse_git_dirty {
#    [[ $(git status 2> /dev/null | tail -n1) != "nothing to commit (working directory clean)" ]] && echo "*"
#}
#
#function parse_git_branch {
#    git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e "s/* \(.*\)/[\1$(parse_git_dirty)]/"
#}

#export PS1='\h:\W$(__git_ps1 "[\[\e[0;32m\]%s\[\e[0m\]\[\e[0;33m\]$(parse_git_dirty)\[\e[0m\]]")$ '

# Shows git branch status in directory
PS1="\h:\W \u\[\033[0;32m\]\$(parse_git_branch_and_add_brackets) \[\033[0m\]\$ "

export SVN_EDITOR=vim
export EDITOR=vim

force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Commands to run when agent forwarding isn't working in this order.
#exec $(ssh-agent)
#ssh-add

if [[ -e $HOME/.sshagent.conf ]]; then
  . $HOME/.sshagent.conf
fi
if `ps -p ${SSH_AGENT_PID} > /dev/null`;then true;
else
  ssh-agent >| $HOME/.sshagent.conf
  . $HOME/.sshagent.conf
  ssh-add ~/.ssh/id_dsa
  ssh-add ~/.ssh/id_rsa_4096
  ssh-add ~/resources/aws/kbasarab.pem
  ssh-add ~/resources/aws/wwedev.pem
fi


PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
